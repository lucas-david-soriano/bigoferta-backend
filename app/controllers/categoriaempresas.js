
module.exports = app => {
    const categoriaempresa    = app.models.categoriaempresas
    const controller = {}

    controller.listarCategoriaempresas = async (req, res) => {
        try{
            const categoriaempresas = await categoriaempresa.find({})
            res.status(200).json({
                status: true,
                data: categoriaempresas
            })
        } catch (err){
            res.status(500).json(err)
        }
    }

    controller.salvaCategoriaempresa = async (req, res) => {
        try{
            const e = await categoriaempresa.create(req.body)
            res.status(200).json({
                status: true,
                data: e
            })
        } catch (err){
            res.status(500).json(err)
        }
    }

    controller.alteraCategoriaempresa = async (req, res) => {
        const _id = req.body._id
        try{
            await categoriaempresa.findByIdAndUpdate(_id, req.body).exec()
            res.status(200).json({status:true, message:"Categoriaempresa alterada com sucesso"})
        } catch (err){
            res.status(500).json(err)
        }
    }

    controller.removeCategoriaempresa = async (req, res) => {
        try{
            await categoriaempresa.remove({_id: req.params.id})
            res.status(200).json({status:true, message:"Categoriaempresa removida com sucesso"})
        } catch (err){
            res.status(500).json(err)
        }
    }

    return controller
}
