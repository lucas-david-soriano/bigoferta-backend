
module.exports = app => {
    const categoriaproduto    = app.models.categoriaprodutos
    const controller = {}

    controller.listarCategoriaprodutos = async (req, res) => {
        try{
            const categoriaprodutos = await categoriaproduto.find({})
            res.status(200).json({
                status: true,
                data: categoriaprodutos
            })
        } catch (err){
            res.status(500).json(err)
        }
    }

    controller.salvaCategoriaproduto = async (req, res) => {
        try{
            const e = await categoriaproduto.create(req.body)
            res.status(200).json({
                status: true,
                data: e
            })
        } catch (err){
            res.status(500).json(err)
        }
    }

    controller.alteraCategoriaproduto = async (req, res) => {
        const _id = req.body._id
        try{
            await categoriaproduto.findByIdAndUpdate(_id, req.body).exec()
            res.status(200).json({status:true, message:"Categoriaproduto alterada com sucesso"})
        } catch (err){
            res.status(500).json(err)
        }
    }

    controller.removeCategoriaproduto = async (req, res) => {
        try{
            await categoriaproduto.remove({_id: req.params.id})
            res.status(200).json({status:true, message:"Categoriaproduto removida com sucesso"})
        } catch (err){
            res.status(500).json(err)
        }
    }

    return controller
}
