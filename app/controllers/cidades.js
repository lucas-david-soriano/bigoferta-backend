
module.exports = app => {
    const cidade    = app.models.cidades
    const controller = {}

    controller.listarCidades = async (req, res) => {
        try{
            const cidades = await cidade.find({}).populate('estado')
            res.status(200).json({
                status: true,
                data: cidades
            })
        } catch (err){
            res.status(500).json(err)
        }
    }

    controller.salvaCidade = async (req, res) => {
        try{
            const e = await cidade.create(req.body)

            if(e) {
                let r = await cidade.findOne(e._id).populate('estado')
                res.status(200).json({
                    status: true,
                    data: r
                })
            }
            
        } catch (err){
            res.status(500).json(err)
        }
    }

    controller.alteraCidade = async (req, res) => {
        const _id = req.body._id
        try{
            await cidade.findByIdAndUpdate(_id, req.body).exec()
            res.status(200).json({status:true, message:"Cidade alterada com sucesso"})
        } catch (err){
            res.status(500).json(err)
        }
    }

    controller.removeCidade = async (req, res) => {
        try{
            await cidade.remove({_id: req.params.id})
            res.status(200).json({status:true, message:"Cidade removida com sucesso"})
        } catch (err){
            res.status(500).json(err)
        }
    }

    return controller
}
