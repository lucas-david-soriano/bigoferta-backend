let dir =  __dirname.replace("controllers", 'uploads/empresas')
const fs = require('fs')
module.exports = app => {
    const empresa    = app.models.empresas
    const controller = {}

    controller.listaEmpresas = async (req, res) => {
        try{
            const dados = await empresa.find({enabled:true}).populate('categoria').exec()
            res.status(200).json(dados)
        } catch (err){
            res.status(500).json(err)
        }
    }   

    controller.salvaEmpresa = async (req, res) => {
        let nomeImg = new Date().getTime() + '.' + req.body.ext
        require("fs").writeFile( `${dir}/${nomeImg}`, req.body.imagem, 'base64', async err => {
            try{
                let decoded = req.decoded
                let data = req.body
                data.logo = nomeImg
                data['usuarios'] = [decoded.user._id]
                await empresa.create(data)
                res.status(201).json({status:true, message:"Empresa cadastrada com sucesso"})
            } catch (err){
                res.status(500).json(err)
            }
        })
    }

    controller.alteraEmpresa = async (req, res) => {
        const _id = req.body._id
        try{
            let e = await empresa.findOne({_id: _id})

            fs.unlink(`${dir}/${e.logo}`, (err) => {
            
                let nomeImg = new Date().getTime() + '.' + req.body.ext

                fs.writeFile( `${dir}/${nomeImg}`, req.body.imagem, 'base64', async err => {
                    let data = req.body
                    data.logo = nomeImg
                    await empresa.findByIdAndUpdate(_id, data).exec()
                    res.status(200).json({status:true, message:"Empresa alterada com sucesso"})

                })
            })

            
        } catch (err){
            console.log(err)
            res.status(500).json(err)
        }
    }

    controller.desativaEmpresa = async (req, res) => {
        try{
            await empresa.findByIdAndUpdate(req.params.id, {enabled: false})
            res.status(200).json({status:true, message:"Empresa desativada com sucesso"})
        } catch (err){
            res.status(500).json(err)
        }
    }

    return controller
}