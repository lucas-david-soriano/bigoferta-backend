
module.exports = app => {
    const estado    = app.models.estados
    const controller = {}

    controller.listarEstados = async (req, res) => {
        try{
            const estados = await estado.find({})
            res.status(200).json({
                status: true,
                data: estados
            })
        } catch (err){
            res.status(500).json(err)
        }
    }

    controller.salvaEstado = async (req, res) => {
        try{
            const e = await estado.create(req.body)
            res.status(200).json({
                status: true,
                data: e
            })
        } catch (err){
            res.status(500).json(err)
        }
    }

    controller.alteraEstado = async (req, res) => {
        const _id = req.body._id
        try{
            await estado.findByIdAndUpdate(_id, req.body).exec()
            res.status(200).json({status:true, message:"Estado alterada com sucesso"})
        } catch (err){
            res.status(500).json(err)
        }
    }

    controller.removeEstado = async (req, res) => {
        try{
            await estado.remove({_id: req.params.id})
            res.status(200).json({status:true, message:"Estado removida com sucesso"})
        } catch (err){
            res.status(500).json(err)
        }
    }

    return controller
}
