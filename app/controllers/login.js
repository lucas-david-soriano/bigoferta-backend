const request  = require("request")
const jwt      = require('jsonwebtoken')
const config = require("./../../config/config.json")
module.exports = app => {
    const usuario    = app.models.usuarios
    const controller = {}

    controller.logar = async (req, res) => {
        request(`https://graph.facebook.com/me?fields=name,picture.type(large),email&access_token=${req.body.token}`, async (err, response, body) => {
            if(err)console.log('err: '+err)
            let {name, id, email, picture} = JSON.parse( body )
            let u = {nome:name, idF: id, email, avatar:picture.data.url}

            try{
                await usuario.updateOne({"facebook_id": id}, u, {upsert: true})
                jwt.sign({u}, config.key, { expiresIn: '3600s' }, (err, token) => {
                    res.status(200).json( { status:true, user: u, token })
                })
            } catch (err){
                res.status(500).json(err)
            }
        })
    }

    return controller
}