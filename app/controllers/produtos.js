let dir =  __dirname.replace("controllers", 'uploads/produtos')
const fs = require("fs")

function checkPermissaoProduto(decoded, p){
    let permissao = false
    for (let i = 0; i < decoded.empresas.length; i++) {
        if(p.empresa.toString() == decoded.empresas[i]._id.toString()){
            permissao = true
        }
    }
    return permissao
}


module.exports = app => {
    const produto    = app.models.produtos
    const controller = {}

    controller.listarProduto = async (req, res) => {
        console.log('aoba')
        try{
            const produtos = await produto.find({},{historico:false}).populate('categoria').populate('empresa').exec()
            res.status(200).json(produtos)
        } catch (err){
            res.status(500).json(err)
        }
    }

    controller.listarProdutoEmpresa = async (req, res) => {
        try{
            const produtos = await produto.find({empresa: req.params.idEmpresa},{historico:false}).populate('categoria').exec()
            res.status(200).json({status: true, data: produtos})
        } catch (err){
            res.status(500).json(err)
        }
    }

    controller.salvaProduto = async (req, res) => {
        let nomeImg = new Date().getTime() + '.' + req.body.ext
        require("fs").writeFile( `${dir}/${nomeImg}`, req.body.imagem, 'base64', async err => {
            let p = req.body
            p.imagem = nomeImg
            try{
                p.historico = []
                p.historico.push({
                    preco:req.body.preco,
                    date: new Date()
                })
                await produto.create(p)
                console.log("salvo")
                res.status(200).json({status:true, message:"Produto cadastrado com sucesso"})
            } catch (err){
                console.log(err)
                res.status(500).json(err)
            }
        })
    }

    controller.alteraProduto = async (req, res) => {
       
        try{

            let p = await produto.findOne({_id: req.body._id})            
            let decoded = req.decoded

            if (checkPermissaoProduto(decoded, p)) {
                fs.unlink(`${dir}/${p.imagem}`, (err) => {
                    let nomeImg = new Date().getTime() + '.' + req.body.ext
                    fs.writeFile( `${dir}/${nomeImg}`, req.body.imagem, 'base64', async err => {
                        console.log(nomeImg)
                        let update = {
                            preco:req.body.preco,
                            date: new Date()
                        }
                        let data = {
                            preco: req.body.preco,
                            nome: req.body.nome,
                            imagem: nomeImg
                        }
                        await produto.findByIdAndUpdate({_id:req.body._id}, data).exec()
                        await produto.findByIdAndUpdate({_id:req.body._id}, {$push: {historico: update } } ).exec()
                        res.status(200).json({status:true, message:"Produto alterado com sucesso"})
                    })
                })
                
            }else{
                res.status(200).json({status:false, message:"Erro ao editar produto"})
            }
        } catch (err){
            res.status(500).json(err)
        }
    }

    controller.desativaProduto = async (req, res) => {
        try{
            await produto.findByIdAndUpdate(req.params.id, {status: false})
            res.status(200).json({status:true, message:"Produto desativado com sucesso"})
        } catch (err){
            res.status(500).json(err)
        }
    }

    return controller
}