module.exports = app => {
    const usuario    = app.models.usuarios
    const controller = {}

    controller.listarUsuario = async (req, res) => {
        try{
            const users = await usuario.find().populate('empresa').exec()
            res.status(200).json(users)
        } catch (err){
            res.status(500).json(err)
        }
    }

    controller.salvaUsuario = async (req, res) => {
        delete req.body.isRoot
        try{
            const u = await usuario.create(req.body)
            res.status(200).json(u)
        } catch (err){
            res.status(500).json(err)
        }
    }

    controller.alteraUsuario = async (req, res) => {
        delete req.body.isRoot
        const _id = req.body._id
        try{
            let a = await usuario.findByIdAndUpdate(_id, req.body).exec()
            res.status(200).json(a)
        } catch (err){
            res.status(500).json(err)
        }
    }

    controller.removeUsuario = async (req, res) => {
        try{
            let a = await usuario.remove({_id: req.params.id})
            res.status(200).json({status:true, message:"removido com sucesso"})
        } catch (err){
            res.status(500).json(err)
        }
    }


    controller.checkUsuario = async (req, res) => {
        try{
            res.status(200).json({status:true, data: req.decoded})
        } catch (err){
            res.status(500).json(err)
        }
    }




    controller.criaRoot = async (req, res) => {
        req.body.isRoot = true
        try{
            const u = await usuario.create(req.body)
            res.status(200).json(u)
        } catch (err){
            res.status(500).json(err)
        }
    }

    return controller
}