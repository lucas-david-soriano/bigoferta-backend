const jwt = require('jsonwebtoken')
const config = require("./../../config/config.json")

module.exports =  (app) => async (req, res, next) => {
    try{
        let decoded = jwt.verify(req.headers.authorization, config.key)
        const modelUsuario    = app.models.usuarios
        const u  = await modelUsuario.findOne({facebook_id: decoded.u.idF}).exec()
        if(u.enabled === false)return res.json({status:false, message:"Usuário desabilitado no serviço"})
        let data           = {}
        data['user']       = u
        const modelEmpresa = app.models.empresas
        const empresas     = await modelEmpresa.find({"usuarios": u._id}).populate('categoria').exec()
        data['empresas']   = empresas
        req['decoded']     = data
        next()
    }catch (e) {
        res.send({
            status: false,
            mensagem: 'Erro na autenticação',
            e
        })
    }
}