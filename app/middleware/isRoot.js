const jwt = require('jsonwebtoken')
const config = require("./../../config/config.json")

module.exports =  (app) => async (req, res, next) => {
    
    try{
        let decoded = jwt.verify(req.headers.authorization, config.key)
        const modelUsuario    = app.models.usuarios
        const u  = await modelUsuario.findOne({facebook_id: decoded.u.idF}).exec()
        
        return u.isRoot === true ? next() : res.json({status:false, message:"sem permissão root"})
    }catch (e) {
        console.log(e)
        res.send({status: false, mensagem: 'Erro na autenticação', e})
    }
}