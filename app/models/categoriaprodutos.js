
const mongoose = require('mongoose')

module.exports = () => {
    const schema = mongoose.Schema({
        nome: {
            type: String,
            required: true
        },
    }, { versionKey: false } )
    schema.set('timestamps', true)
    return mongoose.model('categoriaprodutos', schema)
}
