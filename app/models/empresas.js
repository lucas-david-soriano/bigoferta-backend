const mongoose = require('mongoose')

module.exports = () => {
    
    const schema = mongoose.Schema({
        nome: {
            type: String,
            required: true
        },
        telefones: {
            type: Array
        },
        email: {
            type: Array,
            required: true,
            min: 1
        },
        logo: {
            type: String,
            default: 'sem-imagem.jpg'
        },
        endereco: {
            type: Object,
            required: true
        },
        usuarios: {
            type: Array
        },
        produtos: {
            type: Array
        },
        status: {
            type: Boolean,
            default: false
        },
        categoria: {
            type: mongoose.Schema.ObjectId,
            ref: 'categoriaempresas',
            required: true
        },
        cidade: {
            type: mongoose.Schema.ObjectId,
            ref: 'cidades',
            required: true
        },
        enabled:{
            type:Boolean,
            default:true
        }
    }, { versionKey: false } )
    schema.set('timestamps', true)
    return mongoose.model('empresa', schema)
}