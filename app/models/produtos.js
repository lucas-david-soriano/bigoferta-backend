const mongoose = require('mongoose')

module.exports = () => {
    const schema = mongoose.Schema({
        nome: {
            type: String,
            required: true
        },
        preco: {
            type: Number,
            min: 0,
            required: true
        },
        imagem: {
            type: Object,
            required: true
        },
        status: {
            type: Boolean,
            default: true
        },
        historico: {
            type: Array
        },
        empresa: {
            type: mongoose.Schema.ObjectId,
            ref: 'empresa',
            required: true
        },
        categoria: {
            type: mongoose.Schema.ObjectId,
            ref: 'categoriaprodutos',
            required: true
        }
    }, { versionKey: false } )
    schema.set('timestamps', true)
    return mongoose.model('produto', schema)
}