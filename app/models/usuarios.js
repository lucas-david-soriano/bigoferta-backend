const mongoose = require('mongoose')

module.exports = () => {
    const schema = mongoose.Schema({
        nome: {
            type: String,
            required: true
        },
        email: {
            type: String,
            unique: true,
            required: true
        },
        endereco: {
            type: Object,
            required: false
        },
        avatar: {
            type: String
        },
        facebook_id: {
            type: String
        },
        enabled:{
            type:Boolean,
            default:true
        },
        status:{
            type: Boolean,
            default: true
        },
        isRoot:{
            type: Boolean,
            default: true
        }
    }, { versionKey: false } )
    schema.set('timestamps', true)
    return mongoose.model('usuario', schema)
}