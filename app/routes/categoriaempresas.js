
module.exports = app => {
    const controller = app.controllers.categoriaempresas
    app.get('/categoriaempresa', controller.listarCategoriaempresas)
    app.post('/categoriaempresa', controller.salvaCategoriaempresa)
    app.put('/categoriaempresa', controller.alteraCategoriaempresa)
    app.delete('/categoriaempresa/:id', controller.removeCategoriaempresa)
}
