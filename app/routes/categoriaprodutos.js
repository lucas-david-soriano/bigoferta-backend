
module.exports = app => {
    const isRoot = require("./../middleware/isRoot")

    const controller = app.controllers.categoriaprodutos
    app.get('/categoriaproduto', controller.listarCategoriaprodutos)
    app.post('/categoriaproduto', isRoot(app), controller.salvaCategoriaproduto)
    app.put('/categoriaproduto', isRoot(app), controller.alteraCategoriaproduto)
    app.delete('/categoriaproduto/:id', isRoot(app), controller.removeCategoriaproduto)
}
