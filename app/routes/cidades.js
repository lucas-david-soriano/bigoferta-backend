
module.exports = app => {
    const isRoot = require("./../middleware/isRoot")

    const controller = app.controllers.cidades
    app.get('/cidade', controller.listarCidades)
    app.post('/cidade', isRoot(app), controller.salvaCidade)
    app.put('/cidade', isRoot(app), controller.alteraCidade)
    app.delete('/cidade/:id', isRoot(app), controller.removeCidade)
}
