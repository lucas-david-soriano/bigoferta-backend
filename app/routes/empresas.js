const multer = require('multer')

let storage  = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, __dirname.replace("routes", 'uploads/empresas')  )
  },
  filename: (req, file, cb) => {
    let ext = file.originalname.substr( file.originalname.lastIndexOf('.') + 1 ) 
    cb(null, file.fieldname + '-' + Date.now() +'.'+ext)
  }
})
let upload = multer({ storage: storage })


const authEmpresa = require("./../middleware/authEmpresa")
const isRoot      = require("./../middleware/isRoot")

module.exports = app => {
    const controller = app.controllers.empresas

    app.get('/empresa', controller.listaEmpresas)
    app.post('/empresa', [upload.single('imagem'), authEmpresa(app)], controller.salvaEmpresa)
    app.put('/empresa', isRoot(app), controller.alteraEmpresa)
    app.delete('/empresa/:id', controller.desativaEmpresa)
}