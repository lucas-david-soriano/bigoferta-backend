
module.exports = app => {
    const isRoot = require("./../middleware/isRoot")

    const controller = app.controllers.estados
    app.get('/estado', controller.listarEstados)
    app.post('/estado', isRoot(app), controller.salvaEstado)
    app.put('/estado', isRoot(app), controller.alteraEstado)
    app.delete('/estado/:id', isRoot(app), controller.removeEstado)
}
