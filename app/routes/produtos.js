const multer = require('multer')

let storage  = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, __dirname.replace("routes", 'uploads/produtos')  )
    },
    filename: (req, file, cb) => {
      let ext = file.originalname.substr( file.originalname.lastIndexOf('.') + 1 ) 
      cb(null, file.fieldname + '-' + Date.now() +'.'+ext)
    }
})
let upload = multer({ storage: storage })

const authEmpresa = require("./../middleware/authEmpresa")

module.exports = app => {
    const produto = app.controllers.produtos
    app.get('/produtos', produto.listarProduto)
    app.get('/produtosPorEmpresa/:idEmpresa', produto.listarProdutoEmpresa)

    app.post('/produto', [upload.single('imagem'), authEmpresa(app)], produto.salvaProduto)
    app.put('/produto', authEmpresa(app), produto.alteraProduto)
    app.delete('/produto/:id', produto.desativaProduto)
}