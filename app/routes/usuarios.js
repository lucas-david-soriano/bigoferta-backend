const authEmpresa = require("./../middleware/authEmpresa")
const isRoot      = require("./../middleware/isRoot")

module.exports = app => {
    const usuario = app.controllers.usuarios
    app.get('/usuario', usuario.listarUsuario)
    app.get('/usuario/check', authEmpresa(app), usuario.checkUsuario)
    app.post('/usuario', usuario.salvaUsuario)
    
    app.put('/usuario', usuario.alteraUsuario)
    app.delete('/usuario/:id', usuario.removeUsuario)

    app.post('/criaroot', isRoot(app), usuario.criaRoot)
}