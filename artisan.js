var fs = require('fs');

if (process.argv.length <= 2) {
    console.log("Nome requerido");
    process.exit(-1);
}
 
var nome = process.argv[2].toLowerCase();


const nomeUc = nome.charAt(0).toUpperCase() + nome.slice(1)

const controllerExample = `
module.exports = app => {
    const ${nome}    = app.models.${nome}s
    const controller = {}

    controller.listar${nomeUc}s = async (req, res) => {
        try{
            const ${nome}s = await ${nome}.find({})
            res.status(200).json({
                status: true,
                data: ${nome}s
            })
        } catch (err){
            res.status(500).json(err)
        }
    }

    controller.salva${nomeUc} = async (req, res) => {
        try{
            const e = await ${nome}.create(req.body)
            res.status(200).json({
                status: true,
                data: e
            })
        } catch (err){
            res.status(500).json(err)
        }
    }

    controller.altera${nomeUc} = async (req, res) => {
        const _id = req.body._id
        try{
            await ${nome}.findByIdAndUpdate(_id, req.body).exec()
            res.status(200).json({status:true, message:"${nomeUc} alterada com sucesso"})
        } catch (err){
            res.status(500).json(err)
        }
    }

    controller.remove${nomeUc} = async (req, res) => {
        try{
            await ${nome}.remove({_id: req.params.id})
            res.status(200).json({status:true, message:"${nomeUc} removida com sucesso"})
        } catch (err){
            res.status(500).json(err)
        }
    }

    return controller
}
`
fs.writeFile(`./app/controllers/${nome}s.js`, controllerExample, function (err) {
  if (err) throw err;
  console.log('Controller criado!');
});


const modelExample = `
const mongoose = require('mongoose')

module.exports = () => {
    const schema = mongoose.Schema({
      
    })
    schema.set('timestamps', true)
    return mongoose.model('${nome}s', schema)
}
`

fs.writeFile(`./app/models/${nome}s.js`, modelExample, function (err) {
    if (err) throw err;
    console.log('Model criado!');
});


const routeExample = `
module.exports = app => {
    const controller = app.controllers.${nome}s
    app.get('/${nome}', controller.listar${nomeUc}s)
    app.post('/${nome}', controller.salva${nomeUc})
    app.put('/${nome}', controller.altera${nomeUc})
    app.delete('/${nome}/:id', controller.remove${nomeUc})
}
`

fs.writeFile(`./app/routes/${nome}s.js`, routeExample, function (err) {
    if (err) throw err;
    console.log('Route criado!');
});