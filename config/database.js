const mongoose = require('mongoose')
mongoose.set('debug', false)
module.exports = uri => {
    mongoose.Promise = global.Promise

    mongoose.connect(uri, { useNewUrlParser: true })

    mongoose.connection.on('connected', () =>  {
        console.log('Mongoose! Conectado em '+uri)
    })
    mongoose.connection.on('disconnected', () =>  {
        console.log('Mongoose! Desonectado de '+uri)
    })
    mongoose.connection.on('error', erro => {
        console.log('Mongoose! Erro de conexão: '+erro)
    })

    process.on('SIGINT', () => {
       mongoose.connection.close(() => {
           console.log('Mongoose desconectado pelo término ' +
               'da aplicação')
           // 0 = aplicação finalizada sem erros
           process.exit(0)
       })
    })
}