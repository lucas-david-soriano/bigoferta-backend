const load       = require('express-load')
const bodyParser = require('body-parser')
const express    = require('express')
const jwt        = require('jsonwebtoken')
const config     = require('./config.json')

module.exports = () => {
    const app  = express()

    app.use('/images-loja', express.static(__dirname + '/../app/uploads/empresas/'))
    app.use('/images-produto', express.static(__dirname + '/../app/uploads/produtos/'))


    app.use((req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*')
        res.header('Access-Control-Allow-Methods', 'DELETE, GET, POST, PUT, OPTIONS')
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization')
        res.header('Access-Control-Allow-Credentials', true)

        /*if(req.path == '/auth/facebook' || req.method == "OPTIONS" )return next()

        const bearerHeader = req.headers['authorization']
        
        if(typeof bearerHeader !== 'undefined') 
        {
            const [bearerToken] = bearerHeader.split(' ')
            req.token           = bearerToken
    
            jwt.verify(bearerToken, config.key, (err, authData) => {
                return err ? res.status(403).json({status:false, message:'token inválido'}) : next()      
            })
        } 
        else 
            res.status(403).json({status:false, message:'Token é obrigatório'})*/

            next()
    })

    app.set('port', 3000)
    app.use(bodyParser.urlencoded({
        extended: true,
    }))

    app.use(bodyParser.json({limit: '10mb', extended: true}))
    app.use(bodyParser.urlencoded({limit: '10mb', extended: true}))
    

    app.use(bodyParser.json())
    app.use(require('method-override')())
    load('models', { cwd: 'app'}).then('controllers').then('routes').into(app)
    return app
}
