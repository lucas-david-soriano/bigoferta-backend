const http = require('http')
const app  = require('./config/express')()

require('./config/database')('mongodb://localhost/bigoferta')

http.createServer(app).listen(app.get('port'), () => {
    console.log(`Server escutando na porta ${app.get('port')} `)
})